/* ************************************************************************** */
/*                                                                            */
/*                                                             ::::::::       */
/*   patpat.c                                                :+:    :+:       */
/*                                                          +:+               */
/*   By: nekonoor <nekonoor@protonmail.com>                +#+                */
/*                                                        +#+                 */
/*   Created: 2019/09/02 21:22:35 by nekonoor            #+#    #+#           */
/*   Updated: 2019/09/04 18:26:25 by nekonoor            ########   odam.nl   */
/*                                                                            */
/* ************************************************************************** */

#include "patpat.h"
#include "libuseful.h"
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>

int		main(int ac, char **av)
{
	char	**infiles;
	char	*outpath;
	int		i;

	(void)outpath;
	infiles = (char**)malloc(sizeof(char*) * (ac - 1));
	i = 1;
	while (i < ac)
	{
		infiles[i] = read_file(av[i]);
		uf_putstr(infiles[i]);
		i++;
	}
	return (0);
}

char	*buf_cat(char *out, char *buf)
{
	char	*ret;

	ret = (char*)malloc(sizeof(char) * (uf_strlen(out) + uf_strlen(buf) + 1));
	uf_strcpy(ret, out);
	uf_strcpy(ret + uf_strlen(out), buf);
	free(out);
	return (ret);
}

char	*read_file(char *path)
{
	char	*buf;
	char	*out;
	int		size;
	int		fd;

	buf = (char*)malloc(sizeof(char) * (BUF_SIZE + 1));
	if (buf == NULL)
		return (NULL);
	fd = open(path, O_RDONLY);
	if (errno != 0)
		return (NULL);
	out = (char*)malloc(sizeof(char));
	if (out == NULL)
		return (NULL);
	size = 1;
	while (size)
	{
		size = read(fd, buf, BUF_SIZE);
		if (errno != 0)
			return (NULL);
		buf[size] = '\0';
		out = buf_cat(out, buf);
		if (out == NULL)
			return (NULL);
	}
	close(fd);
	if (errno != 0)
		return (NULL);
	free(buf);
	return (out);
}

/*
**	TODO: write some some sort of data structure to the file
**	showing what bytes are equal in all files and which arent	
**	if(*buff1 == *buff2)
**	{
**		printf("%s == %s\n", buff1, buff2);
**		fputc(buff1[0], (FILE*)fout);
**	}
**	else
**	{
**		printf("%s != %s\n", buff1, buff2);
**		fputc(0x00, (FILE*)fout);
**	}
*/
