/* ************************************************************************** */
/*                                                                            */
/*                                                             ::::::::       */
/*   patpat.h                                                :+:    :+:       */
/*                                                          +:+               */
/*   By: nekonoor <nekonoor@protonmail.com>                +#+                */
/*                                                        +#+                 */
/*   Created: 2019/09/02 21:19:02 by nekonoor            #+#    #+#           */
/*   Updated: 2019/09/02 21:51:28 by nekonoor            ########   odam.nl   */
/*                                                                            */
/* ************************************************************************** */

#ifndef PATPAT_H
# define PATPAT_H
# define BUF_SIZE 4096

char	*buf_cat(char *out, char *buf);
char	*read_file(char *path);

int		ft_strlen(char *str);
void	ft_putstr(char *str);
char	*ft_strcpy(char *dst, char *src);
char	*ft_strdup(char *src);

#endif
